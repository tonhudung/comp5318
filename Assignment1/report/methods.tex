\section{Methods}
\label{sec:methods}

We applied two approaches in solving this problem: 1) Fully connected Deep Neural Network and 2) Convolutional Neural Network 

\subsection{Deep Neural Network}

\subsubsection{Architecture}

A deep neural network is an interconnected group of vertices, inspired by a simplification of neurons in a brain in which, each vertex represents a neuron and the connection between two neurons is assigned a numerical value representing the "strength" of the signal between the two neurons. 

The neurons of the network are organized by layers. One layer connects to neurons from the immediate preceding layer and immediate following layer. The first layer that accepts the input data is called the input layer. The last layer that produces the result is called the output layer. Layers in between the input layer and output layer are called hidden layers. The numerical values of the connections between neurons are called parameters or weights.

\begin{figure}[H]
	\includegraphics[width=0.5\linewidth]{figures/dnn}
	\centering
	\caption{Deep neural network with one hidden layer (Wikipedia)}
	\label{fig:dnn-architecture}
\end{figure}

We started with a network that has the following architecture
\begin{itemize}
	\item Input layer: 784 neurons
	\item 1st hidden layer: 200 neurons
	\item 2nd hidden layer: 150 neurons
	\item 3rd hidden layer: 50 neurons
	\item Output layer: Softmax
\end{itemize}

The intuition behind selecting such architecture is rather simple. We wanted to begin with a small network of just 2 or 3 hidden layers with a reasonable number of neurons to see if it works fine. From the starting architecture, we may want to increase or reduce the complexity of the network if the final model underfits or overfits.

\subsubsection{Data pre-processing}

There are some common forms to preprocess a data matrix X such as \textbf{Mean substraction}, \textbf{Normalization}, \textbf{PCA and Whitening}. In our case, X is a matrix of size [m x n] where m is the number of examples and n is 784.

\paragraph{Mean substraction} is the most common form of data preprocessing. Mean substraction involves subtracting the mean from every individual feature of the data. In the case of image processing, we simply substract the mean value from every pixels. In our code, the implementation in Python is as follows:

\begin{python}[caption=Mean substraction,label=lst:listing-1]
data_train -= np.mean(data_train, axis=1, keepdims=True)
data_test -= np.mean(data_test, axis=1, keepdims=True)
\end{python}

Mean substraction is especially important because consider the case if all the inputs are positive, then the gradients on the weights at that layer will be either all negatives or all positives. That means all the weights must update in the same direction which ultimately lead to non-optimal zig zag path (or longer training time) to reach the optimal point as illustrated in \figurename \ref{fig:zig-zag-path}.

\begin{figure}[H]
	\includegraphics[width=0.5\linewidth]{figures/zig-zag-path}
	\centering
	\caption{Weights updating in the same direction if inputs are all positives}
	\label{fig:zig-zag-path}
\end{figure}

\paragraph{Normalization} sometimes also referred to as feature scaling. The technique involves normalizing the data dimensions so that they are of approximately the same scale. Normalization can be performed in two ways. The first one is to divide each dimension by its standard deviation after mean substraction. In Python, normalization can be implemented as follows:

\begin{python}[caption=Mean substraction,label=lst:listing-1]
	X /= np.std(X, axis = 0)
\end{python}

Another way to normalize is to scale each dimension so that the min and max along the dimension is -1 and 1 respectively. However, it only makes sense to apply normalization on a dataset if different features have different scales, for e.g. area of the house vs price of the house. In the case of image processing, the scale of pixel values are already within similar scale (0-255) so we do not apply normalization.

\paragraph{PCA and Whitening}

In practice, PCA and Whitening are not very common in neural network for image recognition so we skip this method but mention here only for completeness.

\subsubsection{Activation function}

Activation function (or sometimes referred to as transfer function) is a function that defines the output of a neuron given a set of inputs computed by the propagation function to that neuron. The activation function provides non-linearity and allows the network to learn complex representation of data. Some common choices of activation functions are the historically popular Sigmoid function \cite{werbos1974beyond}, the hyperbolic tangent function (Tanh)  \cite{karlik2011performance}, the Rectifier Linear Unit (ReLU) \cite{maas2013rectifier} and its variants such as Leaky ReLU, Noise ReLU and Exponential Linear Units (ELU). 

In practice, ReLU seems to be the most popular choice to be used with neural network so we selected ReLU as a best practice. We described the above mentioned activation functions and the pros and cons of each.

\paragraph{Sigmoid function} is a function that squashes numbers to range [0, 1]. The function was historically due to having nice interpretation as a saturating firing rate of a neuron. The formula for the sigmoid function is given by

$$
S(x) = \frac{e^x}{e^x + 1}
$$ 

and it has the following form as shown in \figurename \ref{fig:sigmoid}

\begin{figure}[H]
	\includegraphics[width=0.5\linewidth]{figures/sigmoid}
	\centering
	\caption{Sigmoid function (Wikipedia)}
	\label{fig:sigmoid}
\end{figure}

It can be observed that the sigmoid function has the upper right and the bottom left region where the gradients are almost zero if the neurons are saturated. In these regions, saturated neurons will cause learning to be very slow because of near zero gradients. Sigmoid function also does not produce zero-centered output so there is an effect of inefficient gradient updates as mentioned above. A minor disadvantage with sigmoid function is that it requires calculation of $exp()$ which can be a bit computationally expensive though it is not quite a problem with modern computers.

\paragraph{Tanh function}

The hyperbolic tangent function is quite similar to the sigmoid function except that it squashes numbers into the range [-1, 1]. It has a zero-centered output so this property is better than sigmoid function but still kills gradients when saturated.

\begin{figure}[H]
	\includegraphics[width=0.5\linewidth]{figures/tanh}
	\centering
	\caption{Hyperbolic tangent function (Wikipedia)}
	\label{fig:tanh}
\end{figure}

\paragraph{ReLU function}

The ReLU function is demonstrated to enable better training of neural network by \cite{glorot2011deep}. The ReLU function is given by the following function, basically it goes through the input element wise and sets to 0 if any input is negative and does nothing otherwise.

$$
 R(x) = x^+ = max(0, x)
$$

ReLU function is by far the most popular activation function for deep neural networks as it solves some of the problems with previously mentioned activation functions. We can see that ReLU does not saturate in the + region. It is highly computationally efficient and sometimes can converge much faster than sigmoid and tanh. The first major convolutional neural network that helped popularized ReLU is the AlexNet in the ImageNet competition held in 2012 \cite{krizhevsky2012imagenet}. They showed that when trained on large scale data, the model with ReLU can sometimes converge up to 6 times faster compared to sigmoid and tanh.

\begin{figure}[H]
	\includegraphics[width=0.5\linewidth]{figures/relu}
	\centering
	\caption{Rectifier linear unit function (Wikipedia)}
	\label{fig:relu}
\end{figure}

However, ReLU still has a region where the gradients may die so there are a number of variants of ReLU such as Leaky ReLU, Maxout and ELU to fix the dying ReLU problem. In practice, people will just start with ReLU and be careful about the learning rates. and optionally try out Leaky ReLU, Maxout or ELU as well as just avoid using sigmoid or tanh. 

In our code, the implementation of ReLU in Python is as follows

\begin{python}
def relu(x):
	return np.maximum(0, x)
\end{python}

\subsubsection{Weight initialization}

A very important starting point in building neural networks is weight initialization. It can be as crucial as seeing the network gradually converge as opposed to not converge at all. We discuss some strategies and their pitfalls as follows

\paragraph{Zero weights initialization}

A reasonable sounding idea about weight initialization is that since we do not know the values of the weights, we may start with a set of zero weights. Turns out, this is something to avoid because if all weights are initialized the same then they essentially compute the same outputs. During back propagation, all the gradients will also be the same so the weights will undergo the same updates.

\paragraph{Random weights initialization}

To avoid the pitfall with zero weight initialization, we should initialize weights with some random values. One way is simply to sample from a multi-dimensional gaussian distribution and scale by a small factor such as 0.01 as shown in the following listing

\begin{python}
W = 0.01 * np.random.rand(R, C)
\end{python}
	
This approach works fine with small networks but experiments show that it has problems with deep networks because as we multiply the activation at each layer with small weight numbers so all the values quickly collapse to zeros. On the backward propagation path, the gradients coming back will also be very small so there will basically no updates.

In contrast, if we scale the sample by a factor of 1 and not 0.01, we observe the opposite result where all neurons are completely saturated to be either -1 or 1.

\paragraph{Xavier initialization}

A reasonable initialization is called Xavier initialization first proposed in \cite{glorot2010understanding}. The intuition is that we want the variance of the input to be the same as the variance of the output. The implementation is given as follows. Basically what it means is that if we have a small number of inputs, then we divide by a small number and we get larger weights so that if we multiply inputs by larger weights then we get same larger variance at output. 

\begin{python}
W = np.random.rand(fan_in, fan_out) / np.sqrt(fan_in)
\end{python}

\paragraph{He initialization}

Xavier initialization, however, breaks when we use it with ReLU activation because of the underlying assumption of linear activation as pointed out in \cite{he2015delving}. He initialization is our option for weight initialization and it is implemented as follows in our code

\begin{python}
def initialize_parameters_he(layer_dims):    
	parameters = {}
	L = len(layer_dims) - 1
	for l in range(1, L + 1):
	parameters['W' + str(l)] = np.random.randn(
		layer_dims[l], layer_dims[l-1]) * np.sqrt(2.0 / layer_dims[l-1])
	parameters['b' + str(l)] = np.zeros((layer_dims[l], 1))
	return parameters
\end{python}


\subsection{Convolutional Neural Network}

\subsubsection{Architecture}

Convolutional neural network is another class of deep neural network mostly applied to computer vision. Unlike a multi layer perceptrons where each neuron in one layer is connected to every neuron in the next layer, hence, fully connected. The CNN has a different approach. In stead of connecting every neuron from one layer to another, the CNN employs small filters to sort of scan through the input matrices at each layer for a small region at a time and perform convo or pooling operation to produce output for the next layer. 

\begin{figure}[H]
	\includegraphics[width=0.5\linewidth]{figures/cnn}
	\centering
	\caption{Convolutional neural network (Wikipedia)}
	\label{fig:cnn}
\end{figure}

As shown in \figurename \ref{fig:cnn}, the general architecture is that the input image will be connected to a convolutional layer, followed by a pooling layer (either max or average pooling). The CNN usually have many blocks of convo + pooling depending on the task at hand. The last few layers of the network have a few fully connected layers and finally end with a soft-max layer for a multi-class classification.

\subsubsection{Building blocks}

\paragraph{Convolutional layer}

The convolutional layer consists of a number of filters. Each filter usually has a small dimension in term of width and height (for e.g. 3x3 or 5.5) but extend through the full depth of the input volume. For example, if the input image is 28x28x3 where 28 is number of pixels for width and height and 3 is the number of channels (RGB) then the filter at the convo layer immediately after the input layer can have dimension of 3x3x3 or 5x5x3. 

During forward path, each filter is convolved across the height and width of the input volume, computing the dot product of the filter and the input producing a 2 dimentional activation map of that filter. When multiple filters are used at the convo layer, multiple 2 dimensional activation maps will be produced as result of convolving multiple filters with the input volume. These activation maps will then be stacked together to produce the input volume for the next layer.

The values of the filters are weights that can be learned through backpropagation.

\paragraph{Pooling layer}

Pooling layer, similarly, uses filters to scan through the input volume, except that its purpose is to reduce the data dimension and effectively reduce computation. The popular choice for the size of the filters at pooling layer is 2x2. At each operation, pooling filter looks at a small region of 2x2 (and also extend through the depth of the input volume), keeping only the max value (for max pooling) or average value (for average pooling).

\paragraph{Fully connected layer}

The fully connected layers are identical to that in a regular multi-layer perceptron. To connect the last pooling layer to a fully connected layer, the input volume at the pooling layer need to be reshaped from 3 dimensional to 2 dimensional.
